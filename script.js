(function(){
    var inputFile = document.getElementById('input');
    var dropZone = document.getElementById('drop-zone');
    //event listener
    inputFile.addEventListener("change", handleFiles, false);
    
    //Select 1 file and create the image inside the drop zone
    function handleFiles() {
        var file = this.files[0];
        var picContainer = document.createElement("div");
        dropZone.appendChild(picContainer);

        var pic = document.createElement("img");
        pic.src = window.URL.createObjectURL(file);
        pic.onload = function(){
            window.URL.revokeObjectURL(this.src);
        }
        picContainer.appendChild(pic);
    }

    //Select multiple files
   /* function handleFiles() {
        var numFiles = this.files.length;
        for(i = 0; i < numFiles; i++){
            console.log(this.files[i]);
        }
    }*/

})();